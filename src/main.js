import Vue from 'vue'
import App from './App.vue'
import element from './plugins/element-ui'
import router from './plugins/router'

Vue.config.productionTip = false

new Vue({
  router,
  element,
  render: (h) => h(App),
}).$mount('#app')
