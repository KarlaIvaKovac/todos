import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: require('../components/Todos.vue').default,
    },
    {
      path: '/blog',
      component: require('../components/Blog.vue').default,
    },
  ],
})

export default router
